/*
****************                               ******************

Ported to mbed platform 
Added Persian Support 

by Arman Safikhani  31 - 08-2015

* ***************                               ******************
*/




#include "TFT_22_ILI9225.h"
#define bitRead(value, bit) (((value) >> (bit)) & 0x01)


int font_color, x_font, y_font;
char highlight, fa_num;
int effect = 0, italic = 0;
unsigned int highlight_color;
/* Global Variables */
int x_text = 0, y_text = 0;
int x_base = 0;



void _orientCoordinates(uint16_t *x1, uint16_t *y1) {

    switch (_orientation) {
    case 0:  // ok
        break;
    case 1: // ok
        *y1 = _maxY - *y1 - 1;
        _swap(x1, *y1);
        break;
    case 2: // ok
        *x1 = _maxX - *x1 - 1;
        *y1 = _maxY - *y1 - 1;
        break;
    case 3: // ok
        *x1 = _maxX - *x1 - 1;
        _swap(x1, *y1);
        break;
    }
}


void _setWindow(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1) {
    _orientCoordinates(x0, y0);
    _orientCoordinates(x1, y1);

    if (x1 < x0) _swap(x0, x1);
    if (y1 < y0) _swap(y0, y1);

    _writeRegister(ILI9225_HORIZONTAL_WINDOW_ADDR1, x1);
    _writeRegister(ILI9225_HORIZONTAL_WINDOW_ADDR2, x0);

    _writeRegister(ILI9225_VERTICAL_WINDOW_ADDR1, y1);
    _writeRegister(ILI9225_VERTICAL_WINDOW_ADDR2, y0);

    _writeRegister(ILI9225_RAM_ADDR_SET1, x0);
    _writeRegister(ILI9225_RAM_ADDR_SET2, y0);

    _writeCommand(0x00, 0x22);
}


void begin(void) {

    // Set up pins

	//already done in main.c

    // Turn on backlight
    //if (_led) _ledInOut = 1;
	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_2,GPIO_PIN_SET);

    // Initialization Code
    //_rstInOut = 1;
	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_1,GPIO_PIN_SET);// Pull the reset pin high to release the ILI9225C from the reset status
    //wait_ms(1);
	HAL_Delay(1);
    //_rstInOut = 0;
	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_1,GPIO_PIN_RESET);// Pull the reset pin low to reset ILI9225
    //wait_ms(10);
	HAL_Delay(10);
    //_rstInOut = 1;
	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_1,GPIO_PIN_SET);// Pull the reset pin high to release the ILI9225C from the reset status
    //wait_ms(50);
    HAL_Delay(50);

    /* Start Initial Sequence */
    /* Set SS bit and direction output from S528 to S1 */
    _writeRegister(ILI9225_POWER_CTRL1, 0x0000); // Set SAP,DSTB,STB
    _writeRegister(ILI9225_POWER_CTRL2, 0x0000); // Set APON,PON,AON,VCI1EN,VC
    _writeRegister(ILI9225_POWER_CTRL3, 0x0000); // Set BT,DC1,DC2,DC3
    _writeRegister(ILI9225_POWER_CTRL4, 0x0000); // Set GVDD
    _writeRegister(ILI9225_POWER_CTRL5, 0x0000); // Set VCOMH/VCOML voltage
    //wait_ms(40);
    HAL_Delay(40);

    // Power-on sequence
    _writeRegister(ILI9225_POWER_CTRL2, 0x0018); // Set APON,PON,AON,VCI1EN,VC
    _writeRegister(ILI9225_POWER_CTRL3, 0x6121); // Set BT,DC1,DC2,DC3
    _writeRegister(ILI9225_POWER_CTRL4, 0x006F); // Set GVDD   /*007F 0088 */
    _writeRegister(ILI9225_POWER_CTRL5, 0x495F); // Set VCOMH/VCOML voltage
    _writeRegister(ILI9225_POWER_CTRL1, 0x0800); // Set SAP,DSTB,STB
    //wait_ms(10);
    HAL_Delay(10);
    _writeRegister(ILI9225_POWER_CTRL2, 0x103B); // Set APON,PON,AON,VCI1EN,VC
    //wait_ms(50);
    HAL_Delay(50);

    _writeRegister(ILI9225_DRIVER_OUTPUT_CTRL, 0x011C); // set the display line number and display direction
    _writeRegister(ILI9225_LCD_AC_DRIVING_CTRL, 0x0100); // set 1 line inversion
    _writeRegister(ILI9225_ENTRY_MODE, 0x1030); // set GRAM write direction and BGR=1.
    _writeRegister(ILI9225_DISP_CTRL1, 0x0000); // Display off
    _writeRegister(ILI9225_BLANK_PERIOD_CTRL1, 0x0808); // set the back porch and front porch
    _writeRegister(ILI9225_FRAME_CYCLE_CTRL, 0x1100); // set the clocks number per line
    _writeRegister(ILI9225_INTERFACE_CTRL, 0x0000); // CPU interface
    _writeRegister(ILI9225_OSC_CTRL, 0x0D01); // Set Osc  /*0e01*/
    _writeRegister(ILI9225_VCI_RECYCLING, 0x0020); // Set VCI recycling
    _writeRegister(ILI9225_RAM_ADDR_SET1, 0x0000); // RAM Address
    _writeRegister(ILI9225_RAM_ADDR_SET2, 0x0000); // RAM Address

                                                   /* Set GRAM area */
    _writeRegister(ILI9225_GATE_SCAN_CTRL, 0x0000);
    _writeRegister(ILI9225_VERTICAL_SCROLL_CTRL1, 0x00DB);
    _writeRegister(ILI9225_VERTICAL_SCROLL_CTRL2, 0x0000);
    _writeRegister(ILI9225_VERTICAL_SCROLL_CTRL3, 0x0000);
    _writeRegister(ILI9225_PARTIAL_DRIVING_POS1, 0x00DB);
    _writeRegister(ILI9225_PARTIAL_DRIVING_POS2, 0x0000);
    _writeRegister(ILI9225_HORIZONTAL_WINDOW_ADDR1, 0x00AF);
    _writeRegister(ILI9225_HORIZONTAL_WINDOW_ADDR2, 0x0000);
    _writeRegister(ILI9225_VERTICAL_WINDOW_ADDR1, 0x00DB);
    _writeRegister(ILI9225_VERTICAL_WINDOW_ADDR2, 0x0000);

    /* Set GAMMA curve */
    _writeRegister(ILI9225_GAMMA_CTRL1, 0x0000);
    _writeRegister(ILI9225_GAMMA_CTRL2, 0x0808);
    _writeRegister(ILI9225_GAMMA_CTRL3, 0x080A);
    _writeRegister(ILI9225_GAMMA_CTRL4, 0x000A);
    _writeRegister(ILI9225_GAMMA_CTRL5, 0x0A08);
    _writeRegister(ILI9225_GAMMA_CTRL6, 0x0808);
    _writeRegister(ILI9225_GAMMA_CTRL7, 0x0000);
    _writeRegister(ILI9225_GAMMA_CTRL8, 0x0A00);
    _writeRegister(ILI9225_GAMMA_CTRL9, 0x0710);
    _writeRegister(ILI9225_GAMMA_CTRL10, 0x0710);

    _writeRegister(ILI9225_DISP_CTRL1, 0x0012);
    //wait_ms(50);
    HAL_Delay(50);
    _writeRegister(ILI9225_DISP_CTRL1, 0x1017);

    setBacklight(true);
    setOrientation(0);

    // Initialize variables
    setBackgroundColor(COLOR_BLACK);

    clear();
}


void clear() {
    uint8_t old = _orientation;
    setOrientation(0);
    fillRectangle(0, 0, _maxX - 1, _maxY - 1, COLOR_BLACK);
    setOrientation(old);
    //wait_ms(10);
    HAL_Delay(10);
}

void fill(uint16_t color)
{
    fillRectangle(0, 0, _maxX - 1, _maxY - 1, color);
}


void invert(bool flag) {
    _writeCommand(0x00, flag ? ILI9225C_INVON : ILI9225C_INVOFF);
}


void setBacklight(bool flag) {
    //if (_led) _ledInOut = flag ? 1 : 0;
	/*
		 	if (_led) {
		 		if (flag) {
		 			_ledInOut = 1;
		 		}
			 	else {
			 		_ledInOut = 0;
			 	}
	 	    }
	*/
	if(flag){
		HAL_GPIO_WritePin(GPIOD,GPIO_PIN_2,GPIO_PIN_SET);
	}
	else{
		HAL_GPIO_WritePin(GPIOD,GPIO_PIN_2,GPIO_PIN_RESET);
	}

}


void setDisplay(bool flag) {
    if (flag) {
        _writeRegister(0x00ff, 0x0000);
        _writeRegister(ILI9225_POWER_CTRL1, 0x0000);
        //wait_ms(50);
        HAL_Delay(50);
        _writeRegister(ILI9225_DISP_CTRL1, 0x1017);
        //wait_ms(200);
        HAL_Delay(200);
    }
    else {
        _writeRegister(0x00ff, 0x0000);
        _writeRegister(ILI9225_DISP_CTRL1, 0x0000);
        //wait_ms(50);
        HAL_Delay(50);
        _writeRegister(ILI9225_POWER_CTRL1, 0x0003);
        //wait_ms(200);
        HAL_Delay(200);
    }
}


void setOrientation(uint8_t orientation) {

    _orientation = orientation % 4;

    switch (_orientation) {
    case 0:
        _maxX = ILI9225_LCD_WIDTH;
        _maxY = ILI9225_LCD_HEIGHT;
        break;
    case 1:
        _maxX = ILI9225_LCD_HEIGHT;
        _maxY = ILI9225_LCD_WIDTH;
        break;
    case 2:
        _maxX = ILI9225_LCD_WIDTH;
        _maxY = ILI9225_LCD_HEIGHT;
        break;
    case 3:
        _maxX = ILI9225_LCD_HEIGHT;
        _maxY = ILI9225_LCD_WIDTH;
        break;
    }
}


uint8_t getOrientation() {
    return _orientation;
}


void drawRectangle(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t color) {
    drawLine(x1, y1, x1, y2, color);
    drawLine(x1, y1, x2, y1, color);
    drawLine(x1, y2, x2, y2, color);
    drawLine(x2, y1, x2, y2, color);
}


void fillRectangle(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t color) {

    _setWindow(x1, y1, x2, y2);

    for (uint16_t t = (y2 - y1 + 1) * (x2 - x1 + 1); t > 0; t--)
        _writeData(color >> 8, color);
}


void drawCircle(uint16_t x0, uint16_t y0, uint16_t r, uint16_t color) {

    int16_t f = 1 - r;
    int16_t ddF_x = 1;
    int16_t ddF_y = -2 * r;
    int16_t x = 0;
    int16_t y = r;

    drawPixel(x0, y0 + r, color);
    drawPixel(x0, y0 - r, color);
    drawPixel(x0 + r, y0, color);
    drawPixel(x0 - r, y0, color);

    while (x < y) {
        if (f >= 0) {
            y--;
            ddF_y += 2;
            f += ddF_y;
        }
        x++;
        ddF_x += 2;
        f += ddF_x;

        drawPixel(x0 + x, y0 + y, color);
        drawPixel(x0 - x, y0 + y, color);
        drawPixel(x0 + x, y0 - y, color);
        drawPixel(x0 - x, y0 - y, color);
        drawPixel(x0 + y, y0 + x, color);
        drawPixel(x0 - y, y0 + x, color);
        drawPixel(x0 + y, y0 - x, color);
        drawPixel(x0 - y, y0 - x, color);
    }
}


void fillCircle(uint8_t x0, uint8_t y0, uint8_t radius, uint16_t color) {

    int16_t f = 1 - radius;
    int16_t ddF_x = 1;
    int16_t ddF_y = -2 * radius;
    int16_t x = 0;
    int16_t y = radius;

    while (x < y) {
        if (f >= 0) {
            y--;
            ddF_y += 2;
            f += ddF_y;
        }
        x++;
        ddF_x += 2;
        f += ddF_x;

        drawLine(x0 + x, y0 + y, x0 - x, y0 + y, color); // bottom
        drawLine(x0 + x, y0 - y, x0 - x, y0 - y, color); // top
        drawLine(x0 + y, y0 - x, x0 + y, y0 + x, color); // right
        drawLine(x0 - y, y0 - x, x0 - y, y0 + x, color); // left
    }
    fillRectangle(x0 - x, y0 - y, x0 + x, y0 + y, color);
}


void drawLine(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t color) {

    // Classic Bresenham algorithm
    int16_t steep = abs(y2 - y1) > abs(x2 - x1);
    int16_t dx, dy;

    if (steep) {
        _swap(x1, y1);
        _swap(x2, y2);
    }

    if (x1 > x2) {
        _swap(x1, x2);
        _swap(y1, y2);
    }

    dx = x2 - x1;
    dy = abs(y2 - y1);

    int16_t err = dx / 2;
    int16_t ystep;

    if (y1 < y2) ystep = 1;
    else ystep = -1;


    for (; x1 <= x2; x1++) {
        if (steep) drawPixel(y1, x1, color);
        else       drawPixel(x1, y1, color);

        err -= dy;
        if (err < 0) {
            y1 += ystep;
            err += dx;
        }
    }
}


void drawPixel(uint16_t x1, uint16_t y1, uint16_t color) {

    if ((x1 >= _maxX) || (y1 >= _maxY)) return;

    _setWindow(x1, y1, x1 + 1, y1 + 1);
    _orientCoordinates(x1, y1);
    _writeData(color >> 8, color);
    //HAL_GPIO_TogglePin(GPIOB,GPIO_PIN_14);
}


uint16_t maxX() {
    return _maxX;
}


uint16_t maxY() {
    return _maxY;
}


uint16_t setColor(uint8_t red8, uint8_t green8, uint8_t blue8) {
    // rgb16 = red5 green6 blue5
    return (red8 >> 3) << 11 | (green8 >> 2) << 5 | (blue8 >> 3);
}


void splitColor(uint16_t rgb, uint8_t *red, uint8_t *green, uint8_t *blue) {
    // rgb16 = red5 green6 blue5
    
    red = (rgb & 0xF800) >> 11 << 3;
    green = (rgb & 0x7E0) >> 5 << 2;
    blue = (rgb & 0x1F) << 3;
}


void _swap(uint16_t *a, uint16_t *b) {
    uint16_t w = a;
    a = b;
    b = w;
}

// Utilities
void _writeCommand(uint8_t HI, uint8_t LO) {
    //_rsInOut = 0;
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_0, GPIO_PIN_RESET);
    //_csInOut = 0;
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET);
    //spi.write(HI);
	uint16_t datasize = 8;
    //HAL_SPI_Transmit_IT(&hspi1,&HI,datasize);
	HAL_SPI_Transmit(&hspi1,&HI,datasize,SPI_TIMEOUT);
    //spi.write(LO);
    //HAL_SPI_Transmit_IT(&hspi1,&LO,datasize);
	HAL_SPI_Transmit(&hspi1,&LO,datasize,SPI_TIMEOUT);
    //_csInOut = 1;
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET);
}


void _writeData(uint8_t HI, uint8_t LO) {
    //_rsInOut = 1;
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_0, GPIO_PIN_SET);
    //_csInOut = 0;
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_RESET);
    //spi.write(HI);
	uint16_t datasize = 8;
    //HAL_SPI_Transmit_IT(&hspi1,&HI,datasize);
	HAL_SPI_Transmit(&hspi1,&HI,datasize,SPI_TIMEOUT);
    //spi.write(LO);
    //HAL_SPI_Transmit_IT(&hspi1,&LO,datasize);
	HAL_SPI_Transmit(&hspi1,&LO,datasize,SPI_TIMEOUT);
    //_csInOut = 1;
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_4, GPIO_PIN_SET);
}


void _writeRegister(uint16_t reg, uint16_t data) {
    _writeCommand(reg >> 8, reg & 255);
    _writeData(data >> 8, data & 255);
}


void drawTriangle(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t x3, uint16_t y3, uint16_t color) {
    drawLine(x1, y1, x2, y2, color);
    drawLine(x2, y2, x3, y3, color);
    drawLine(x3, y3, x1, y1, color);
}


void fillTriangle(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t x3, uint16_t y3, uint16_t color) {

    uint16_t a, b, y, last;

    // Sort coordinates by Y order (y3 >= y2 >= y1)
    if (y1 > y2) {
        _swap(y1, y2); _swap(x1, x2);
    }
    if (y2 > y3) {
        _swap(y3, y2); _swap(x3, x2);
    }
    if (y1 > y2) {
        _swap(y1, y2); _swap(x1, x2);
    }

    if (y1 == y3) { // Handle awkward all-on-same-line case as its own thing
        a = b = x1;
        if (x2 < a)      a = x2;
        else if (x2 > b) b = x2;
        if (x3 < a)      a = x3;
        else if (x3 > b) b = x3;
        drawLine(a, y1, b, y1, color);
        return;
    }

    uint16_t    dx11 = x2 - x1,
        dy11 = y2 - y1,
        dx12 = x3 - x1,
        dy12 = y3 - y1,
        dx22 = x3 - x2,
        dy22 = y3 - y2,
        sa = 0,
        sb = 0;

    // For upper part of triangle, find scanline crossings for segments
    // 0-1 and 0-2.  If y2=y3 (flat-bottomed triangle), the scanline y2
    // is included here (and second loop will be skipped, avoiding a /0
    // error there), otherwise scanline y2 is skipped here and handled
    // in the second loop...which also avoids a /0 error here if y1=y2
    // (flat-topped triangle).
    if (y2 == y3) last = y2;   // Include y2 scanline
    else          last = y2 - 1; // Skip it

    for (y = y1; y <= last; y++) {
        a = x1 + sa / dy11;
        b = x1 + sb / dy12;
        sa += dx11;
        sb += dx12;
        /* longhand:
        a = x1 + (x2 - x1) * (y - y1) / (y2 - y1);
        b = x1 + (x3 - x1) * (y - y1) / (y3 - y1);
        */
        if (a > b) _swap(a, b);
        drawLine(a, y, b, y, color);
    }

    // For lower part of triangle, find scanline crossings for segments
    // 0-2 and 1-2.  This loop is skipped if y2=y3.
    sa = dx22 * (y - y2);
    sb = dx12 * (y - y1);
    for (; y <= y3; y++) {
        a = x2 + sa / dy22;
        b = x1 + sb / dy12;
        sa += dx22;
        sb += dx12;
        /* longhand:
        a = x2 + (x3 - x2) * (y - y2) / (y3 - y2);
        b = x1 + (x3 - x1) * (y - y1) / (y3 - y1);
        */
        if (a > b) _swap(a, b);
        drawLine(a, y, b, y, color);
    }
}


void setBackgroundColor(uint16_t color) {
	if(color == NULL){
		color = COLOR_BLACK;
	}
    _bgColor = color;
}


/*
void setFont(uint8_t* font) {

    cfont.font = font;
    cfont.width = font[0];
    cfont.height = font[1];
    cfont.offset = font[2];
    cfont.numchars = font[3];
    cfont.nbrows = cfont.height / 8;

    if (cfont.height % 8) cfont.nbrows++;  // Set number of bytes used by height of font in multiples of 8
}


void drawText(uint16_t x, uint16_t y, char *s, uint16_t color) {

	if(color == NULL){
		color = COLOR_WHITE;
	}
    uint16_t currx = x;

    // Print every character in string
    for (uint8_t k = 0; k < strlen(s); k++) {
        currx += drawChar(currx, y, s[k], color) + 1;
    }
}


uint16_t drawChar(uint16_t x, uint16_t y, uint16_t ch, uint16_t color) {
	if(color == NULL){
		color = COLOR_WHITE;
	}
    uint8_t charData, charWidth;
    uint8_t h, i, j;
    uint16_t charOffset;

    charOffset = (cfont.width * cfont.nbrows) + 1;  // bytes used by each character
    charOffset = (charOffset * (ch - cfont.offset)) + FONT_HEADER_SIZE;  // char offset (add 4 for font header)
    charWidth = cfont.font[charOffset];  // get font width from 1st byte
    charOffset++;  // increment pointer to first character data byte

    for (i = 0; i <= charWidth; i++) {  // each font "column" (+1 blank column for spacing)
        h = 0;  // keep track of char height
        for (j = 0; j < cfont.nbrows; j++) {  // each column byte
            if (i == charWidth) charData = (uint8_t)0x0; // Insert blank column
            else                charData = cfont.font[charOffset];
            charOffset++;

            // Process every row in font character
            for (uint8_t k = 0; k < 8; k++) {
                if (h >= cfont.height) break;  // No need to process excess bits
                if (bitRead(charData, k)) drawPixel(x + i, y + (j * 8) + k, color);
                else                      drawPixel(x + i, y + (j * 8) + k, _bgColor);
                h++;
            };
        };
    };
    return charWidth;
}


//----------------------------------------------------------------------------------------------------
//************************************* ECA 2.8 inch LCD Module **************************************
//----------------------------------------------------------------------------------------------------
// Set Farsi font color
//----------------------------------------------------------------------------------------------------
void setFontColor(int color)
{
    font_color = color;
}
//----------------------------------------------------------------------------------------------------
//************************************* ECA 2.8 inch LCD Module **************************************
//----------------------------------------------------------------------------------------------------
// Set number format to farsi
//----------------------------------------------------------------------------------------------------
void setNumberFormatToFa(void)
{
    fa_num = 1;
}

//----------------------------------------------------------------------------------------------------
//************************************* ECA 2.8 inch LCD Module **************************************
//----------------------------------------------------------------------------------------------------
// Set number format to english
//----------------------------------------------------------------------------------------------------
void setNumberFormatToEn(void)
{
    fa_num = 0;
}

//----------------------------------------------------------------------------------------------------
//************************************* ECA 2.8 inch LCD Module **************************************
//----------------------------------------------------------------------------------------------------
// Set efect of the farsi font
// ef : effect number           ef:1,2,...,5
//----------------------------------------------------------------------------------------------------
void setFontEffect(int ef)
{
    if (ef >= 0 && ef <= 5)
        effect = ef;
}

//----------------------------------------------------------------------------------------------------
//************************************* ECA 2.8 inch LCD Module **************************************
//----------------------------------------------------------------------------------------------------
// Enable Italic effect for farsi font 
//----------------------------------------------------------------------------------------------------
void setItalicOn(void)
{
    italic = 1;
}

//----------------------------------------------------------------------------------------------------
//************************************* ECA 2.8 inch LCD Module **************************************
//----------------------------------------------------------------------------------------------------
// Disable Italic effect for farsi font 
//----------------------------------------------------------------------------------------------------
void setItalicOff(void)
{
    italic = 0;
}
*/
//----------------------------------------------------------------------------------------------------
//************************************* ECA 2.8 inch LCD Module **************************************
//----------------------------------------------------------------------------------------------------
// Puts a Farsi Character On The LCD Screen      simlpe
//----------------------------------------------------------------------------------------------------
/*
void putChar(char character)
{
    //x_text
    int counter_x, counter_y;
    char letter_length, flag_p;

    letter_length = font12x16_FA[character][0] >> 12;
    for (counter_y = 0; counter_y<16; counter_y++)
        for (counter_x = 0; counter_x<letter_length; counter_x++)
        {
            flag_p = font12x16_FA[character][counter_y] >> (counter_x);
            flag_p = flag_p & 0x01;
            if (flag_p)
                drawPixel(_maxX - 3 - (x_font + (counter_x)), y_font + counter_y, font_color);
            else if (highlight)
                drawPixel(_maxX - 3 - (x_font + (counter_x)), y_font + counter_y, highlight_color);
        }

    x_font += letter_length;
    if (x_font > _maxX - 1)
    {
        x_font = x_base;
        y_font += 16;
        if (y_font > _maxY - 0)
            y_font = 0;
    }

}
*/

//----------------------------------------------------------------------------------------------------
//************************************* ECA 2.8 inch LCD Module **************************************
//----------------------------------------------------------------------------------------------------
// Description  : Draws a beveled figure on the screen. 
// Input        : x0, y0 - coordinate position of the upper left center
//                  : x1, y1 - coordinate position of the lower right center
//              : rad    - defines the redius of the circle,
//              : fill   - fill yes or no
//----------------------------------------------------------------------------------------------------
/*
void roundRectangle(int x0, int y0, int x1, int y1, int rad, bool fill, int color)
{
    signed int a, b, P;

    a = 0;                      // increment by 1
    b = rad;                // decrement by 1 using P
    P = 1 - rad;


    if (fill)
    {
        fillRectangle(x0, y0 + rad, x1, y1 - rad, color);

        do
        {
            fillRectangle(x0 - a + rad, y0 - b + rad, a + x1 - rad, y0 - b + rad, color);   // 8 --> 1
            fillRectangle(x0 - b + rad, y0 - a + rad, b + x1 - rad, y0 - a + rad, color);   // 7 --> 2
            fillRectangle(x0 - b + rad, a + y1 - rad, b + x1 - rad, a + y1 - rad, color);   // 6 --> 3
            fillRectangle(x0 - a + rad, b + y1 - rad, a + x1 - rad, b + y1 - rad, color);   // 5 --> 4

            if (P < 0)
                P += 3 + 2 * a++;
            else
                P += 5 + 2 * (a++ - b--);

        } while (a <= b);
    } //fill
    else
    {
        fillRectangle(x0 + rad, y0, x1 - rad, y0, color);   // top
        fillRectangle(x0 + rad, y1, x1 - rad, y1, color);   // bottom
        fillRectangle(x0, y0 + rad, x0, y1 - rad, color);   // left
        fillRectangle(x1, y0 + rad, x1, y1 - rad, color);   // right

        do
        {
            drawPixel(a + x1 - rad, y0 - b + rad, color);   // `````` Segment 1
            drawPixel(b + x1 - rad, y0 - a + rad, color);   // `````` Segment 2

            drawPixel(b + x1 - rad, a + y1 - rad, color);   // `````` Segment 3
            drawPixel(a + x1 - rad, b + y1 - rad, color);   // `````` Segment 4

            drawPixel(x0 - a + rad, b + y1 - rad, color);   // `````` Segment 5
            drawPixel(x0 - b + rad, a + y1 - rad, color);   // `````` Segment 6

            drawPixel(x0 - b + rad, y0 - a + rad, color);   // `````` Segment 7
            drawPixel(x0 - a + rad, y0 - b + rad, color);   // `````` Segment 8

            if (P < 0)
                P += 3 + 2 * a++;
            else
                P += 5 + 2 * (a++ - b--);
        } while (a <= b);
    } //no fill
}   //RoundRectangle

*/



//----------------------------------------------------------------------------------------------------
//************************************* ECA 2.8 inch LCD Module **************************************
//----------------------------------------------------------------------------------------------------
// Puts a Farsi Character On The LCD Screen with scaling
// size : Scale factor       size:1,2,3,...
//---------------------------------------------------------------------------------------------------

/*
void putBoldChar(char character, int size)
{
    int counter_x, counter_y, i, j;
    char letter_length, flag_p;


    letter_length = font12x16_FA[character][0] >> 12;
    for (counter_y = 0; counter_y<16; counter_y++)
        for (counter_x = 0; counter_x<letter_length; counter_x++)
        {
            flag_p = font12x16_FA[character][counter_y] >> (counter_x);
            flag_p = flag_p & 0x01;


            if (flag_p)
            {

                if (effect == 0)
                    for (i = 0; i<size; i++)
                        for (j = 0; j<size; j++)
                            drawPixel(_maxX - 3 + italic*(-counter_y + 7) + i - (x_font + (size*counter_x)), y_font + size*counter_y + j, font_color);
                // Very good effect (1)
                else if (effect == 1)
                {
                    // good for size = 2
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - (x_font + (size*counter_x)), y_font + size*counter_y, font_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - (x_font + (size*counter_x)), y_font + size*counter_y - 1, font_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - (x_font + (size*counter_x)), y_font + size*counter_y + 1, font_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - 1 - (x_font + (size*counter_x)), y_font + size*counter_y, font_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) + 1 - (x_font + (size*counter_x)), y_font + size*counter_y, font_color);
                }
                else if (effect == 2)
                {
                    // good for size = 2
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - (x_font + (size*counter_x)), y_font + size*counter_y, font_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - (x_font + (size*counter_x)), y_font + size*counter_y - 1, font_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - (x_font + (size*counter_x)), y_font + size*counter_y + 1, font_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - 1 - (x_font + (size*counter_x)), y_font + size*counter_y + 1, font_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) + 1 - (x_font + (size*counter_x)), y_font + size*counter_y + 1, font_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - 1 - (x_font + (size*counter_x)), y_font + size*counter_y, font_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) + 1 - (x_font + (size*counter_x)), y_font + size*counter_y, font_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - (x_font + (size*counter_x)), y_font + size*counter_y + 2, font_color);
                }
                else if (effect == 3)
                {
                    // good for size = 3
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - (x_font + (size*counter_x)), y_font + size*counter_y, font_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - (x_font + (size*counter_x)), y_font + size*counter_y - 1, font_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) + 1 - (x_font + (size*counter_x)), y_font + size*counter_y - 1, font_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - 1 - (x_font + (size*counter_x)), y_font + size*counter_y, font_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) + 1 - (x_font + (size*counter_x)), y_font + size*counter_y, font_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) + 2 - (x_font + (size*counter_x)), y_font + size*counter_y, font_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - (x_font + (size*counter_x)), y_font + size*counter_y + 1, font_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) + 1 - (x_font + (size*counter_x)), y_font + size*counter_y + 1, font_color);
                }
                else if (effect == 4)
                {
                    // good for size = 3
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - (x_font + (size*counter_x)), y_font + size*counter_y, font_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - (x_font + (size*counter_x)), y_font + size*counter_y - 1, font_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) + 1 - (x_font + (size*counter_x)), y_font + size*counter_y - 1, font_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - (x_font + (size*counter_x)), y_font + size*counter_y + 1, font_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - 1 - (x_font + (size*counter_x)), y_font + size*counter_y + 1, font_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) + 1 - (x_font + (size*counter_x)), y_font + size*counter_y + 1, font_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) + 2 - (x_font + (size*counter_x)), y_font + size*counter_y + 1, font_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - 1 - (x_font + (size*counter_x)), y_font + size*counter_y, font_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) + 1 - (x_font + (size*counter_x)), y_font + size*counter_y, font_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) + 2 - (x_font + (size*counter_x)), y_font + size*counter_y, font_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - (x_font + (size*counter_x)), y_font + size*counter_y + 2, font_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) + 1 - (x_font + (size*counter_x)), y_font + size*counter_y + 2, font_color);
                }
                else if (effect == 5)
                {
                    // good for size = 1,3
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - (x_font + (size*counter_x)), y_font + size*counter_y, font_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - (x_font + (size*counter_x)), y_font + size*counter_y + 1, font_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) + 1 - (x_font + (size*counter_x)), y_font + size*counter_y, font_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) + 1 - (x_font + (size*counter_x)), y_font + size*counter_y + 1, font_color);
                }

                //tftlcd_draw_circle(_maxX - 3 -(x_font+(size*counter_x)),y_font+size*counter_y,size,1,font_color);

            }
            else if (highlight)
            {


                if (effect == 0)
                    for (i = 0; i<size; i++)
                        for (j = 0; j<size; j++)
                            drawPixel(_maxX - 3 + italic*(-counter_y + 7) + i - (x_font + (size*counter_x)), y_font + size*counter_y + j, highlight_color);
                // Very good effect (1)
                else if (effect == 1)
                {
                    // good for size = 2
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - (x_font + (size*counter_x)), y_font + size*counter_y, font_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - (x_font + (size*counter_x)), y_font + size*counter_y - 1, highlight_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - (x_font + (size*counter_x)), y_font + size*counter_y + 1, highlight_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - 1 - (x_font + (size*counter_x)), y_font + size*counter_y, highlight_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) + 1 - (x_font + (size*counter_x)), y_font + size*counter_y, highlight_color);
                }
                else if (effect == 2)
                {
                    // good for size = 2
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - (x_font + (size*counter_x)), y_font + size*counter_y, highlight_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - (x_font + (size*counter_x)), y_font + size*counter_y - 1, highlight_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - (x_font + (size*counter_x)), y_font + size*counter_y + 1, highlight_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - 1 - (x_font + (size*counter_x)), y_font + size*counter_y + 1, highlight_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) + 1 - (x_font + (size*counter_x)), y_font + size*counter_y + 1, highlight_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - 1 - (x_font + (size*counter_x)), y_font + size*counter_y, highlight_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) + 1 - (x_font + (size*counter_x)), y_font + size*counter_y, highlight_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - (x_font + (size*counter_x)), y_font + size*counter_y + 2, highlight_color);
                }
                else if (effect == 3)
                {
                    // good for size = 3
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - (x_font + (size*counter_x)), y_font + size*counter_y, highlight_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - (x_font + (size*counter_x)), y_font + size*counter_y - 1, highlight_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) + 1 - (x_font + (size*counter_x)), y_font + size*counter_y - 1, highlight_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - 1 - (x_font + (size*counter_x)), y_font + size*counter_y, highlight_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) + 1 - (x_font + (size*counter_x)), y_font + size*counter_y, highlight_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) + 2 - (x_font + (size*counter_x)), y_font + size*counter_y, highlight_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - (x_font + (size*counter_x)), y_font + size*counter_y + 1, highlight_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) + 1 - (x_font + (size*counter_x)), y_font + size*counter_y + 1, highlight_color);
                }
                else if (effect == 4)
                {
                    // good for size = 3
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - (x_font + (size*counter_x)), y_font + size*counter_y, highlight_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - (x_font + (size*counter_x)), y_font + size*counter_y - 1, highlight_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) + 1 - (x_font + (size*counter_x)), y_font + size*counter_y - 1, highlight_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - (x_font + (size*counter_x)), y_font + size*counter_y + 1, highlight_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - 1 - (x_font + (size*counter_x)), y_font + size*counter_y + 1, highlight_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) + 1 - (x_font + (size*counter_x)), y_font + size*counter_y + 1, highlight_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) + 2 - (x_font + (size*counter_x)), y_font + size*counter_y + 1, highlight_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - 1 - (x_font + (size*counter_x)), y_font + size*counter_y, highlight_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) + 1 - (x_font + (size*counter_x)), y_font + size*counter_y, highlight_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) + 2 - (x_font + (size*counter_x)), y_font + size*counter_y, highlight_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - (x_font + (size*counter_x)), y_font + size*counter_y + 2, highlight_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) + 1 - (x_font + (size*counter_x)), y_font + size*counter_y + 2, highlight_color);
                }
                else if (effect == 5)
                {
                    // good for size = 1,3
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - (x_font + (size*counter_x)), y_font + size*counter_y, highlight_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) - (x_font + (size*counter_x)), y_font + size*counter_y + 1, highlight_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) + 1 - (x_font + (size*counter_x)), y_font + size*counter_y, highlight_color);
                    drawPixel(_maxX - 3 + italic*(-counter_y + 7) + 1 - (x_font + (size*counter_x)), y_font + size*counter_y + 1, highlight_color);
                }



                //tftlcd_draw_circle(_maxX - 3 + italic*(-counter_y+7) -(x_font+(size*counter_x)),y_font+size*counter_y,size,1,highlight_color);


            }
        }

    x_font += size*letter_length;
    if (x_font > _maxX - 1)
    {
        x_font = x_base;
        y_font += size * 16;
        if (y_font > _maxY - 0)
            y_font = 0;
    }

}*/


//----------------------------------------------------------------------------------------------------
//************************************* ECA 2.8 inch LCD Module **************************************
//----------------------------------------------------------------------------------------------------
// Go to a specific pont for farsi font (x:0..TS_SIZE_X , y:0..TS_SIZE_Y)
//----------------------------------------------------------------------------------------------------
/*
void goToXY(int x, int y)
{
    if ((x >= _maxX) || (x < 0))
    {
        x_font = 0;
        x_base = 0;
    }
    else
    {
        x_font = x;
        x_base = x;
    }

    if ((y >= _maxY) || (y < 0))
        y_font = 0;
    else
        y_font = y;
}

//----------------------------------------------------------------------------------------------------
//************************************* ECA 2.8 inch LCD Module **************************************
//----------------------------------------------------------------------------------------------------
// Enable Farsi font Highlight
//----------------------------------------------------------------------------------------------------
void setTextHighlightOn(void)
{
    highlight = 1;
}

//----------------------------------------------------------------------------------------------------
//************************************* ECA 2.8 inch LCD Module **************************************
//----------------------------------------------------------------------------------------------------
// Disable Farsi font Highlight
//----------------------------------------------------------------------------------------------------
void setTextHighlightOff(void)
{
    highlight = 0;
}

//----------------------------------------------------------------------------------------------------
//************************************* ECA 2.8 inch LCD Module **************************************
//----------------------------------------------------------------------------------------------------
// Set Farsi font Highlight color
//----------------------------------------------------------------------------------------------------
void setTextHighlightColor(unsigned int color)
{
    highlight_color = color;
}
*/
//----------------------------------------------------------------------------------------------------
//************************************* ECA 2.8 inch LCD Module **************************************
//----------------------------------------------------------------------------------------------------
// Show a farsi-englisg string on the LCD with specific size
// size : Scale factor       size:1,2,3,...
//----------------------------------------------------------------------------------------------------


/*
void putMixedEnAndFaString(const unsigned char *string, int size)
{
    unsigned char letter, nt_letter, pr_letter;
    unsigned char CN = 0, CP = 0, pr_CN = 0, nt_CP = 0, place = 0, flag1 = 0;
    unsigned char letter_counter = 0, letter_count = 0, length = 0, enter = 0;
    unsigned char text_buffer[32];


    flag1 = 0;

    while (*string)
    {
        if (*string > 0x80)
        {
            while (*string > 0x80)
            {
                if (flag1 == 0)
                    pr_letter = 0x20;
                else
                    pr_letter = *(string - 1);

                letter = *string++;

                if (*string == 0)
                    nt_letter = 0x20;
                else
                    nt_letter = *string;

                flag1 = 1;

                if (letter > 0x98) letter = letter;
                else if (letter == 0x98) letter = 0xBC;
                else if (letter == 0x90) letter = 0xC0;
                else if (letter == 0x8D) letter = 0xBE;
                else if (letter == 0x81) letter = 0xBD;
                else if (letter == 0x8E) letter = 0xBF;

                if (pr_letter > 0x98) pr_letter = pr_letter;
                else if (pr_letter == 0x98) pr_letter = 0xBC;
                else if (pr_letter == 0x90) pr_letter = 0xC0;
                else if (pr_letter == 0x8D) pr_letter = 0xBE;
                else if (pr_letter == 0x81) pr_letter = 0xBD;
                else if (pr_letter == 0x8E) pr_letter = 0xBF;

                if (nt_letter > 0x98) nt_letter = nt_letter;
                else if (nt_letter == 0x98) nt_letter = 0xBC;
                else if (nt_letter == 0x90) nt_letter = 0xC0;
                else if (nt_letter == 0x8D) nt_letter = 0xBE;
                else if (nt_letter == 0x81) nt_letter = 0xBD;
                else if (nt_letter == 0x8E) nt_letter = 0xBF;



                if (pr_letter > 0x80)
                    pr_CN = ((FAmap[(pr_letter - 0xBC)][5]) == 1);
                else
                    pr_CN = 0;

                if (nt_letter > 0x80)
                    nt_CP = ((FAmap[(nt_letter - 0xBC)][4]) == 1);
                else
                    nt_CP = 0;

                if (letter > 0x80)
                    CP = ((FAmap[(letter - 0xBC)][4]) == 1);
                else
                    CP = 0;

                if (letter > 0x80)
                    CN = ((FAmap[(letter - 0xBC)][5]) == 1);
                else
                    CN = 0;

                CP = pr_CN && CP;
                CN = CN && nt_CP;
                place = (CP << 1) | CN;

                text_buffer[letter_counter++] = FAmap[(letter - 0xBC)][place];

                length += size*(font12x16_FA[FAmap[(letter - 0xBC)][place]][1] >> 12);
            }



            if (length < _maxX - x_font - 3)
                for (letter_count = 0; letter_count<letter_counter; letter_count++)
                    putBoldChar(text_buffer[letter_count], size);
            else
            {

                x_font = x_base;
                y_font += size * 16;
                if (y_font > _maxY - size * 16)
                    y_font = 0;

                for (letter_count = 0; letter_count<letter_counter; letter_count++)
                    putBoldChar(text_buffer[letter_count], size);
            }
            letter_counter = 0;
            length = 0;
        }
        else if (*string == 0x20)   //Space detect
            putBoldChar(*string++, size);
        else   //English letter & Number & Enter detect
        {
            if (*string == 0x0D)   //Enter detect
            {
                x_font = x_base;
                y_font += size * 16;
                if (y_font > _maxY - size * 16)
                    y_font = 0;

                string += 2;
                goto p1;
            }
            //letter_counter = 0;
            while ((*string < 0x81) && (*(string + 1) < 0x81) && (*string != 0x00))
            {

                if (fa_num)
                {
                    if ((*string > 0x2F) && (*string < 0x3A) && (*string != 0x00))
                    {
                        letter = (*string) - 0x20;
                        text_buffer[letter_counter++] = letter;
                        string++;
                        goto P2;
                    }
                }
                text_buffer[letter_counter++] = *string++;
            P2:
                if ((*string == 0x20) && ((letter_counter * size * 8) < (_maxX - x_font - 1)))
                    flag1 = letter_counter;

                if ((letter_counter * size * 8) > (_maxX - x_font - 1))
                {
                    string -= (letter_counter - flag1 + 1);
                    letter_counter = flag1 - 1;
                    enter = 1;

                    break;
                }

            }
            if ((letter_counter * size * 8) < (_maxX - x_font - 1))
            {
                for (letter_count = (letter_counter); letter_count>0; letter_count--)
                    putBoldChar(text_buffer[letter_count - 1], size);

                if (enter)
                {
                    enter = 0;
                    x_font = x_base;
                    y_font += size * 16;
                    if (y_font > _maxY - size * 16)
                        y_font = 0;
                }
            }
        p1:
            letter_counter = 0;
            flag1 = 0;

        }
    }
    flag1 = 0;

}
*/
//----------------------------------------------------------------------------------------------------
//************************************* ECA 2.8 inch LCD Module **************************************
//----------------------------------------------------------------------------------------------------
// Make an ascii string from an unicode string 
//----------------------------------------------------------------------------------------------------

/*
void unicode2ascii(char *uni_str, char *ascii_str)
{
    int counter = 0;
    int Uch = 0;
    char chl, chh;


    while (*uni_str)
    {
        chl = *uni_str++;
        chh = *uni_str++;

        Uch = 0;
        Uch = ((Uch | chh) << 8) | chl;

        if (Uch > 1574 && Uch < 1591)
            *(ascii_str + counter) = (char)(Uch - 1376);
        else if (Uch > 1590 && Uch < 1595)
            *(ascii_str + counter) = (char)(Uch - 1375);
        else if (Uch > 1600 && Uch < 1603)
            *(ascii_str + counter) = (char)(Uch - 1380);
        else if (Uch == 1705)
            *(ascii_str + counter) = (char)(Uch - 1482);
        else if (Uch == 1604)
            *(ascii_str + counter) = (char)(Uch - 1379);
        else if (Uch > 1604 && Uch < 1609)
            *(ascii_str + counter) = (char)(Uch - 1378);
        else if (Uch == 1740)
            *(ascii_str + counter) = (char)(Uch - 1503);
        else if (Uch == 1574)
            *(ascii_str + counter) = (char)(Uch - 1381);
        else if (Uch == 1662)
            *(ascii_str + counter) = (char)(Uch - 1533);
        else if (Uch == 1670)
            *(ascii_str + counter) = (char)(Uch - 1529);
        else if (Uch == 1688)
            *(ascii_str + counter) = (char)(Uch - 1546);
        else if (Uch == 1711)
            *(ascii_str + counter) = (char)(Uch - 1567);
        else if (Uch == 1570)
            *(ascii_str + counter) = (char)(Uch - 1376);
        else if (Uch > 1631 && Uch < 1642)
            *(ascii_str + counter) = (char)(Uch - 1584);
        else if (Uch == 65536)
            *(ascii_str + counter) = NULL;
        else
            *(ascii_str + counter) = (char)Uch;


        counter++;

    }
    *(ascii_str + counter) = NULL;

}

*/

